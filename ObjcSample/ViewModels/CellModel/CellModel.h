//
//  CellModel.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CellModelDelegate.h"

@interface CellModel : NSObject

@property (nonatomic, strong) id dataObject;
@property (nonatomic, strong) UIImage * _Nullable image;
@property (nonatomic, weak) id <CellModelDelegate> delegate;

- (void)loadImage;

@end
