//
//  CellModel.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "CellModel.h"
#import "ImageLoader.h"
#import "DataProviderDelegate.h"
#import "ImageLinkContainer.h"

@interface CellModel()<DataProviderDelegate>

@property (nonatomic, strong) ImageLoader* imageLoader;
@property (strong, nonatomic) NSString *link;

@end

@implementation CellModel

- (ImageLoader *)imageLoader {
    if (!_imageLoader) {
        _imageLoader = [[ImageLoader alloc] initWithLink: self.link delegate: self];
    }
    return _imageLoader;
}

- (void)loadImage {
    if ([self.dataObject respondsToSelector:@selector(imageLink)]) {
        self.link = [self.dataObject imageLink];
        [self.imageLoader loadImage];
    } else {
        NSLog(@"No link");
    }
}

#pragma mark - DataProviderDelegate
- (void)didStartLoading {
    //Ignore
}

- (void)didFinishLoadingWithError:(NSError *)error {
    self.image = [UIImage imageNamed:@"WarningIcon"];
    [self.delegate modelDidUpdate];
}

- (void)didFinishLoadingWithSuccess:(id)result {
    if ([result isKindOfClass:[UIImage class]]) {
        self.image = result;
    } else {
        self.image = [UIImage imageNamed:@"WarningIcon"];
    }
    
    [self.delegate modelDidUpdate];
}

@end
