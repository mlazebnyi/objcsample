//
//  DownloadImageRequest.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/22/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestManager.h"

@interface DownloadImageRequest : NSObject

+ (void)downloadImageWithURL:(NSURL *)url completionHandler:(RequestCompletionHandler _Nonnull)handler;

@end
