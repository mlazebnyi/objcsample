//
//  DownloadImageRequest.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/22/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "DownloadImageRequest.h"
#import <UIKit/UIKit.h>

@implementation DownloadImageRequest

+ (void)downloadImageWithURL:(NSURL *)url completionHandler:(RequestCompletionHandler _Nonnull)handler {
    NSURLSessionDownloadTask *downloadPhotoTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       
                                                       if (error) {
                                                           Response *result = [[Response alloc] initWithError:error];
                                                           handler(result);
                                                           return;
                                                       }
                                                       
                                                       UIImage *downloadedImage = [UIImage imageWithData:
                                                                                   [NSData dataWithContentsOfURL:location]];
                                                       Response *result = [[Response alloc] initWithResponse:downloadedImage];
                                                       handler(result);
                                                   }];
    
    // 4
    [downloadPhotoTask resume];
}

@end
