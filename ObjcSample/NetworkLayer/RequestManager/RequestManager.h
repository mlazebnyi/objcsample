//
//  RequestManager.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"

typedef void (^RequestCompletionHandler)(Response *response);

@interface RequestManager : NSObject

+ (void)fetchListForPage:(NSString * _Nullable)link completionHandler:(RequestCompletionHandler)handler;

@end
