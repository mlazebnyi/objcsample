//
//  RequestManager.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "RequestManager.h"

@interface RequestManager()

+ (NSString *)baseLink;

@end

@implementation RequestManager

+ (NSString *)baseLink {
    return @"https://rickandmortyapi.com/api/";
}

+ (void)fetchListForPage:(NSString * _Nullable)pageLink completionHandler:(RequestCompletionHandler _Nonnull)handler {
    
    NSString *link = [[RequestManager baseLink] stringByAppendingString: @"/character/"];
    if (pageLink) {
        link = pageLink;
    }
    
    NSURL *url = [NSURL URLWithString:link];
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error != nil) {
            Response *aResponse = [[Response alloc] initWithError: error];
            handler(aResponse);
            return;
        }
        
        NSError *parseError = nil;
        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        
        if (parseError != nil) {
            Response *aResponse = [[Response alloc] initWithError: parseError];
            handler(aResponse);
            return;
        }

        Response *aResponse = [[Response alloc] initWithResponse: responseDictionary];
        handler(aResponse);
        
    }];
    [dataTask resume];
}

@end
