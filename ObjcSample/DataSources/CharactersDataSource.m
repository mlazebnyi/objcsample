//
//  CharactersDataSource.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "CharactersDataSource.h"
#import "CellModel.h"
#import "TableViewCell.h"

@interface CharactersDataSource() <UITableViewDelegate, UITableViewDataSource>

@end

@implementation CharactersDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cellModels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:TableViewCell.reuseIdentifier
                                                          forIndexPath:indexPath];
    CellModel *model = [self.cellModels objectAtIndex: indexPath.row];
    [cell loadWithModel:model];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 190;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == [self.cellModels count] - 1) {
        //TODO: Notify delegate (view controller) to load next page
    }
}

@end
