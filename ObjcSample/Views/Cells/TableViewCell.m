//
//  TableViewCell.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "TableViewCell.h"
#import "CharacterModel.h"

@interface TableViewCell()<CellModelDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *characterImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *speciesLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@property (strong, nonatomic) CellModel *model;

@end

@implementation TableViewCell

+ (NSString *)reuseIdentifier {
    return @"TableViewCell";
}

- (void)loadWithModel:(CellModel *)model {
    self.model.delegate = nil;
    
    self.model = model;
    self.model.delegate = self;
    
    if (![model.dataObject isKindOfClass: [CharacterModel class]]) {
        self.nameLabel.text = @"Error";
        return;
    }
    CharacterModel *character = model.dataObject;
    self.nameLabel.text = [NSString stringWithFormat:@"Name: %@", character.name];
    self.statusLabel.text = [NSString stringWithFormat:@"Status: %@", character.status];
    self.speciesLabel.text = [NSString stringWithFormat:@"Species: %@", character.species];
    self.locationLabel.text = [NSString stringWithFormat:@"Location: %@", character.location];
    
    if (self.model.image) {
        [self.characterImageView setImage: model.image];
    } else {
        [self.characterImageView setImage: nil];
        [self.model loadImage];
    }
}

- (void)modelDidUpdate {
    if (self.model.image) {
        [self.characterImageView setImage: self.model.image];
    }
}

@end
