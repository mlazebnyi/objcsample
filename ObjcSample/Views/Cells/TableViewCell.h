//
//  TableViewCell.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellModelLoadable.h"

@interface TableViewCell<CellModelLoadable> : UITableViewCell

+ (NSString *)reuseIdentifier;
- (void)loadWithModel:(CellModel *)model;

@end
