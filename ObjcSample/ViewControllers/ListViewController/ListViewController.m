//
//  ListViewController.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "ListViewController.h"
#import "CharactersDataProvider.h"
#import "CharactersDataSource.h"
#import "CharacterModel.h"
#import "CellModel.h"

@interface ListViewController () <DataProviderDelegate>

@property (strong, nonatomic) IBOutlet CharactersDataSource *dataSource;
@property (nonatomic, strong) CharactersDataProvider* dataProvider;
@property (nonatomic) NSString * _Nullable nextPage;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadNextPage];
}

- (void)loadNextPage {
    if (!self.dataProvider) {
        self.dataProvider = [[CharactersDataProvider alloc] initWithDelegate:self];
    }
    
    [self.dataProvider getCaractersForPage: self.nextPage];
}

#pragma mark - DataProviderDelegate
- (void)didStartLoading {
    //TODO: Show activity indicator
}

- (void)didFinishLoadingWithError:(NSError *)error {
    //TODO: Show error message
    NSLog(@"Error: %@", error);
}


//TODO: Add metas to the DataProviderDelegate.didFinishLoadingWithSuccess: to support pagination
- (void)didFinishLoadingWithSuccess:(id)result {
    if ([result isKindOfClass: [NSArray class]]) {
        NSMutableArray *models = [[NSMutableArray alloc] init];
        for (CharacterModel *character in result) {
            CellModel *model = [[CellModel alloc] init];
            model.dataObject = character;
            [models addObject:model];
        }
        self.dataSource.cellModels = models;
        [self.dataSource.tableView reloadData];
    }
}

@end
