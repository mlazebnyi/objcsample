//
//  BaseModel.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

#pragma mark - Mappable methods

+ (NSDictionary *)mappingDictionary {
    NSAssert(NO, @"Method 'mappingDictionary' must be overriden");
    return nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)dataDictionary {
    self = [super init];
    if (self) {
        [self updateWithDictionary:dataDictionary];
    }
    
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)data {
    [[self.class mappingDictionary]  enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSString *prop = [data valueForKeyPath:key];
        if (prop && ![prop isKindOfClass:[NSNull class]]) {
            [self mapIncommingProperty:prop forKey:obj];
        }
    }];
}

- (void)mapIncommingProperty:(id)prop forKey:(NSString *)key {
    if ([prop isKindOfClass:[NSArray class]]) {
        //TODO: do linking
    } else if ([prop isKindOfClass:[NSDictionary class]]) {
        //TODO: do linking
    } else {
        [self setValue:prop forKey:key];
    }
}

@end
