//
//  Response.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Response : NSObject

@property (nonatomic, readonly) Boolean isError;

@property (nonatomic, strong, readonly) NSError* _Nullable error;
@property (nonatomic, strong, readonly) id _Nullable response;

- (instancetype)initWithError:(NSError * _Nonnull)error;
- (instancetype)initWithResponse:(id _Nonnull)response;

@end
