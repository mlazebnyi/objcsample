//
//  Response.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "Response.h"

@interface Response ()

@property (nonatomic, strong) NSError* _Nullable error;
@property (nonatomic, strong) id _Nullable response;

@end

@implementation Response

- (instancetype)init
{
    NSAssert(NO, @"Use other available init methods");
    return nil;
}

- (instancetype)initWithError:(NSError * _Nonnull)error {
    self = [super init];
    if (self) {
        self.error = error;
    }
    return self;
}

- (instancetype)initWithResponse:(id _Nonnull)response {
    self = [super init];
    if (self) {
        self.response = response;
    }
    return self;
}

- (Boolean)isError {
    return self.error != nil;
}

@end
