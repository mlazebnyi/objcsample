//
//  CharacterModel.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "CharacterModel.h"


const struct CharacterFields CharacterFields = {
    .identifier = @"identifier",
    .name = @"name",
    .status = @"status",
    .species = @"species",
    .location = @"location.name",
    .imageLink = @"image",
};

@implementation CharacterModel

#pragma mark - Mappable Protocol

+ (NSDictionary *)mappingDictionary {
    return @{
             CharacterFields.identifier : NSStringFromSelector(@selector(identifier)),
             CharacterFields.name       : NSStringFromSelector(@selector(name)),
             CharacterFields.status     : NSStringFromSelector(@selector(status)),
             CharacterFields.species    : NSStringFromSelector(@selector(species)),
             CharacterFields.location   : NSStringFromSelector(@selector(location)),
             CharacterFields.imageLink  : NSStringFromSelector(@selector(imageLink)),
             };
}


@end
