//
//  CharacterModel.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

extern const struct CharacterFields {
    __unsafe_unretained NSString *identifier;
    __unsafe_unretained NSString *name;
    __unsafe_unretained NSString *status;
    __unsafe_unretained NSString *species;
    __unsafe_unretained NSString *location;
    __unsafe_unretained NSString *imageLink;
} CharacterFields;

@interface CharacterModel<ImageLinkContainer> : BaseModel

@property NSString *identifier;
@property NSString *name;
@property NSString *status;
@property NSString *species;
@property NSString *location;
@property NSString *imageLink;

@end
