//
//  ImageLoader.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/22/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "ImageLoader.h"
#import "DownloadImageRequest.h"

@interface ImageLoader()

@property (strong, nonatomic) NSString * _Nonnull link;
@property (nonatomic, weak) id <DataProviderDelegate> delegate;

@end

@implementation ImageLoader

- (instancetype)initWithLink:(NSString * _Nonnull)link delegate:(id<DataProviderDelegate> _Nonnull)delegate {
    self = [super init];
    if (self) {
        self.link = link;
        self.delegate = delegate;
    }
    return self;
}

- (void)loadImage {
    NSURL *url = [[NSURL alloc] initWithString: self.link];
    
    PCTWeakSelf
    [DownloadImageRequest downloadImageWithURL:url completionHandler:^(Response *response) {
        if (response.isError) {
            [weakSelf.delegate didFinishLoadingWithError: response.error];
            return;
        }
        if (![response.response isKindOfClass: [UIImage class]]) {
            NSError *anError = [NSError errorWithDomain:@"domain"
                                                   code:1001
                                               userInfo:@{NSLocalizedDescriptionKey:@"Incorrect image data"}];
            [weakSelf.delegate didFinishLoadingWithError: anError];
            return;
        }
        
        PCTStrongSelf
        dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf.delegate didFinishLoadingWithSuccess: response.response];
        });
    }];
    
}

@end
