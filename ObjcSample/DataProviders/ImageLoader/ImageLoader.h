//
//  ImageLoader.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/22/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataProviderDelegate.h"
#import <UIKit/UIKit.h>

@interface ImageLoader : NSObject

@property (strong, nonatomic, readonly) NSString *link;

- (instancetype)initWithLink:(NSString * _Nonnull)link delegate:(id<DataProviderDelegate> _Nonnull)delegate;
- (void)loadImage;

@end
