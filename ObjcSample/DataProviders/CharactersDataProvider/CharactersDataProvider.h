//
//  CharactersDataProvider.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataProviderDelegate.h"

@interface CharactersDataProvider : NSObject

@property (nonatomic) NSString * _Nullable page;

- (instancetype)initWithDelegate:(id<DataProviderDelegate>)delegate;
- (void)getCaractersForPage:(NSString * _Nullable)page;

@end
