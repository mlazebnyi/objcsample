//
//  CharactersDataProvider.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "CharactersDataProvider.h"
#import "RequestManager.h"
#import "CharacterModel.h"

@interface CharactersDataProvider()

@property (nonatomic, weak) id <DataProviderDelegate> delegate;

@end

@implementation CharactersDataProvider

- (instancetype)initWithDelegate:(id<DataProviderDelegate>)delegate {
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}

- (void)getCaractersForPage:(NSString * _Nullable)page {
    [self.delegate didStartLoading];
    
    PCTWeakSelf
    [RequestManager fetchListForPage:page completionHandler:^(Response *response) {
        if (response.isError) {
            [weakSelf.delegate didFinishLoadingWithError: response.error];
            return;
        }
        if (![response.response isKindOfClass: [NSDictionary class]]) {
            NSError *anError = [NSError errorWithDomain:@"domain"
                                                   code:1001
                                               userInfo:@{NSLocalizedDescriptionKey:@"Incorrect request response"}];
            [weakSelf.delegate didFinishLoadingWithError: anError];
            return;
        }
        
        NSMutableArray *characters = [[NSMutableArray alloc] init];
        for (NSDictionary *dict in [response.response objectForKey: @"results"]) {
            CharacterModel *character = [[CharacterModel alloc] initWithDictionary:dict];
            [characters addObject:character];
        }
        
        PCTStrongSelf
        dispatch_async(dispatch_get_main_queue(), ^{
            [strongSelf.delegate didFinishLoadingWithSuccess: characters];
        });
    }];
}

@end
