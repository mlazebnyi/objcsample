//
//  ImageLinkContainer.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/22/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImageLinkContainer <NSObject>

- (NSString *)imageLink;

@end
