//
//  Mappable.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Mappable <NSObject>

+ (NSDictionary *)mappingDictionary

@end
