//
//  CellModelLoadable.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import "CellModel.h"

@protocol CellModelLoadable <NSObject>

- (void)loadWithModel:(CellModel *)model;

@end
