//
//  DataProviderDelegate.h
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataProviderDelegate <NSObject>

- (void)didStartLoading;
- (void)didFinishLoadingWithError:(NSError *)error;
- (void)didFinishLoadingWithSuccess:(id)result;

@end
