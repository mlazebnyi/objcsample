//
//  RequestManager.m
//  ObjcSample
//
//  Created by Maksym Lazebnyi on 4/21/18.
//  Copyright © 2018 Maksym Lazebnyi. All rights reserved.
//

#ifndef PhysCult_PCTDefines_h
#define PhysCult_PCTDefines_h

#define PCTWeakSelf __weak typeof(self) weakSelf = self;
#define PCTStrongSelf __strong typeof(weakSelf) strongSelf = weakSelf;

#endif
